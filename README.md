__KDE 2021 RPHL__
*KDE Neon*

That repository comes alongside a full guide on my [medium.com page](https://medium.com/tech-notes-and-geek-stuff/kde-plasma-a-complete-setup-guide-8cb253d081a5), with step by step tutorial for installing Zsh and Vim + plugins.


![Preview KDE](preview_kde2021.png "Preview / Rphl")

## 1. Softwares

- Applications details:

__TYPE__  | __PROGRAM__ | __CONFIG__
----------|-------------|-----------
__Web Browser__|Brave|[Website](https://brave.com/fr/download/)
__Shell__|Zsh|[.zshrc](https://gitlab.com/rphl_m/kde-neon/tree/master/.zshrc)
__Terminal__|Konsole & Qterminal|*apt*
__Text editor__|Vim + Plugins|[vimrc](https://gitlab.com/rphl_m/kde-neon/tree/master/.vimrc)
__Text editor__|Kate|default
__System Info__|Neofetch|[config.conf](https://gitlab.com/rphl_m/kde-neon/tree/master/neofetch/config.conf)
__File Manager cli__|Ranger|*apt*
__Music player cli__|Cmus|*apt* 
__Music Player__|Clementine|*apt*
__Music Tag__|EsayTag|*apt* (Kid3 just doesn't work)
__Color picker__|kcolorchooser|*apt*
__Backup__|Kup-backup|*apt*
__Search tool__|Kfind|*apt*
__Search tool cli__|Fzf/Fd|*apt*
__Exa (ls replacement)__|exa|*apt*
__Scan__|Skanlite|*apt*
__Partition Manager__|Gparted|*apt* (KDE partition manager is not reliable)

### Install  Packages
```
sudo apt install catimg clementine cmus easytag encfs fd-find filezilla fzf gimp git gparted grep gufw htop imagemagick k3b kcolorchooser kfile kget krita ktorrent kup-backup libreoffice libreoffice-kde5 neofetch nmap ranger ripgrep skanlite ttf-mscorefonts-installer vim virtualbox zsh
```

### Install Zshell + plugins
[Installation guide](https://medium.com/tech-notes-and-geek-stuff/kde-plasma-a-complete-setup-guide-8cb253d081a5)

### Install Vim + plugins
[Installation guide](https://medium.com/tech-notes-and-geek-stuff/kde-plasma-a-complete-setup-guide-8cb253d081a5)

## 2. Appearance
__Type__  | __Program__ | __Config / Install__
----------|-------------|-----------
Font|Ubuntu Medium 9 pt|system
Mono Font|JetBrains Mono Nerd Font 11 pt|[Github](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts)
Cursor Theme|Bibata-Modern-Ice|community
Theme & Icons|Breeze Dark|default

__Alternatives__  | __Program__ | __Config / Install__
----------|-------------|-----------
Plasma theme|Breeze Darc|community
Window decoration|Breeze Chameleon|community
Color theme|Breeze Darc|community
Icon theme|Papirus-Folders 'breeze'|[repo](sudo add-apt-repository ppa:papirus/papirus)
Icon theme|We10x-Dark|[repo](https://github.com/yeyushengfan258/We10X-icon-theme)

__Improve font settings for GTK apps__
Modify the file '.config/gtk-3.0/settings.ini' to tweak your font, icon theme and mouse theme. If GTK apps font render remains glitchy (aliased / unstable), try this:
- Settings > Fonts > Force PPP to __110__ and Font Hinting to __Slight__
- Choose a smooth font like __Ubuntu 9pt__ (Noto and Roboto don't display perfectly on Plasma 5.20/21)

## 3. Nvidia drivers (nvidia/intel GPU)
Install nvidia drivers from repo (`nvidia-drivers-xxx`) and use this trick to avoid the SDDM resolution/black screen bug :
- delete your `/etc/X11/xorg.conf` file (mv to xorg.conf.bak)
- add these 3 lines to `/usr/share/sddm/scripts/Xsetup`

```
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
xrandr --dpi 96
```
