#!/bin/bash
#kde-neon DOTFILES SAVE

# COPY DOTFILES
echo "-- GITLAB PROJECT 'kde-neon' --"
read -p "[1/2] Copy config files ? (y/n): " repcopy
if [ "$repcopy" = "y" ]
then
    #files
	cp ~/.zshrc ~/Public/kde-neon/.zshrc
	cp ~/.config/neofetch/config.conf ~/Public/kde-neon/neofetch/config.conf
	cp ~/.vimrc ~/Public/kde-neon/.vimrc
	cp ~/.config/ranger/rc.conf ~/Public/kde-neon/ranger/rc.conf
    
    #folders
	cp -avr ~/bin/* ~/Public/kde-neon/bin/
	sleep 1s
else
	exit
fi

# COMMIT / PUSH
read -p "[2/2] Push to Git ? (y/n): " reppush
if [ "$reppush" = "y" ]
then
	read -r -p "Message:" gitdesc
	cd ~/Public/kde-neon/ ; git add . ; git commit -m "$gitdesc" ; git push -u origin master
	sleep 1s
else
    exit
fi
