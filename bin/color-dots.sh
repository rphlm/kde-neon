#!/bin/bash
# Term Colors
FGNAMES=('' '' '' '' '' '' '' '')
for b in $(seq 0 0); do
    if [ "$b" -gt 0 ]; then
      bg=$(($b+39))
    fi
    for f in $(seq 0 7); do
      echo -en "\033[${bg}m\033[$(($f+30))m ${FGNAMES[$f]} "
    done
  echo -e "\033[0m"  
echo
done
