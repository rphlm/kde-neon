#!/bin/bash
clear
menu () {	# action menu 1
   echo -e $"\n \033[41;1m COMMANDS ----------- \033[0m"
   echo " 1. Create /bin folder"
   echo " 2. Clone Git"
   echo " 3. Copy config files"
   echo " 4. Push to Git"
   echo " x. Exit"
   echo -e "\033[31m --------------------- \033[0m"
}
menu		# enter choice and execute 
while read -p " Choice: " CHOIX; do
  case $CHOIX in

    1)
	echo -e $"\033[0;33m mkdir -p ~/bin/ \033[0m" ; mkdir -p ~/bin/ ;;
    2)
	echo -e $"\033[0;33m git clone https://gitlab.com/rphl_m/xfce_conf.git \033[0m"
	cd ~/bin/ ; git clone https://gitlab.com/rphl_m/xfce_conf.git
	cd ~/bin/xfce_conf/ ; git init ; git remote add origin git@gitlab.com:rphl_m/xfce_conf.git ;;
    3)
	cp ~/.conkyrc ~/bin/xfce_conf/.conkyrc
	cp ~/.compton.conf ~/bin/xfce_conf/.compton.conf
	cp ~/.bashrc ~/bin/xfce_conf/.bashrc
	cp ~/.zshrc ~/bin/xfce_conf/.zshrc
	cp ~/.oh-my-zsh/themes/robbyrussell.zsh-theme ~/bin/xfce_conf/robbyrussell.zsh-theme
	cp ~/.config/neofetch/config.conf ~/bin/xfce_conf/neofetch_config.conf
	cp ~/.config/rofi/config ~/bin/xfce_conf/rofi_config
	cp ~/bin/* ~/bin/xfce_conf/
	sleep 1s
	echo "Copying files:" 
	cd ~/bin/ && printf "\033[0;32m > %s\n" * 
	printf "\033[0m Done"
	;; 
    4)
	read -r -p " Message: " gitdesc
	cd ~/bin/xfce_conf/ ; git add . ; git commit -m "$gitdesc" ; git push -u origin master ;;
    x)
	break ;;
    *)
      echo -e "\033[0;31m Unknown command \033[0m" && sleep 1s ;;
  esac
  menu
done
